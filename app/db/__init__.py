from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from core.config import settings

engine = create_engine(settings.DATABASE_URL, executemany_mode='batch')

SessionLocal = sessionmaker(bind=engine)

Base = declarative_base()
