from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from db import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    first_name = Column(String)
    last_name = Column(String)
    age = Column(Integer)


class Quiz(Base):
    __tablename__ = "quiz"

    id = Column(Integer, primary_key=True, index=True)


class Question(Base):
    __tablename__ = "question"

    id = Column(Integer, primary_key=True, index=True)
    question = Column(String, nullable=False)

    # right_answer_id = Column(Integer, ForeignKey('answer.id'), nullable=False)
    answers = relationship("Answer", back_populates='question', primaryjoin="Question.id==Answer.question_id")

    quiz_id = Column(Integer, ForeignKey('quiz.id'), nullable=False)
    quiz = relationship('Quiz', foreign_keys=[quiz_id])

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Answer(Base):
    __tablename__ = "answer"

    id = Column(Integer, primary_key=True, index=True)
    text = Column(String, nullable=False)

    question_id = Column(Integer, ForeignKey('question.id'))
    question = relationship('Question', back_populates="answers")
