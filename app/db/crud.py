from sqlalchemy.orm import Session, lazyload
from db import models


def get_quiz_list(db: Session):
    return db.query(models.Quiz).all()


def get_quiz_by_id(db: Session, quiz_id):
    return db.query(models.Quiz).filter_by(id=quiz_id).first()


def get_question_by_id(db: Session, quiz_id):
    return db.query(models.Quiz).filter_by(id=quiz_id).first()


def get_question_by_quiz_id_list(db: Session, quiz_id):
    return db.query(models.Question).filter_by(quiz_id=quiz_id).options(lazyload(models.Question.answers))
