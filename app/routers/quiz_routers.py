from typing import List

from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session
from db import crud, models
import schema
from dependencies import get_db
from core.config import settings

router = APIRouter(prefix=f"{settings.API_V1_STR}", tags=['Api'])


@router.get("/")
async def root():
    return {"message": "Hello World"}


@router.get('/quiz/', response_model=List[schema.Quiz])
async def get_quiz_list(db: Session = Depends(get_db)):
    return crud.get_quiz_list(db)


@router.get('/quiz/{quiz_id}')
async def get_quiz_by_id(quiz_id: int, db: Session = Depends(get_db)):
    print('*' * 100)
    # db.query(exists().where(models.Quiz.id == quiz_id))  #
    if_exists = crud.get_quiz_by_id(db, quiz_id)

    if if_exists:
        question_list = []
        questions_obj = crud.get_question_by_quiz_id_list(db, quiz_id).all()

        if questions_obj:
            for question in questions_obj:
                question_obj = {
                    'id': question.id,
                    'question': question.question,
                    'answers': question.answers
                }
                question_list.append(question_obj)

        return question_list
    else:
        return JSONResponse(status_code=404, content={'message': "Quiz not found"})


@router.post('/quiz/', response_model=schema.Quiz)
def create_quiz(quiz: schema.Quiz, db: Session = Depends(get_db)):
    quiz_obj = models.Quiz(**quiz.dict())
    db.add(quiz_obj)
    db.commit()
    db.refresh(quiz_obj)
    return quiz


@router.post('/question/', response_model=schema.Question)
def create_question(question: schema.Question, db: Session = Depends(get_db)):
    question_obj = models.Question(
        question=question.question,
        quiz_id=question.quiz_id
    )
    db.add(question_obj)
    db.flush()
    db.refresh(question_obj)

    answer_list = []
    for answer in question.answers:
        answer_list.append(models.Answer(**answer.dict(), question_id=question_obj.id))

    db.bulk_save_objects(answer_list)

    db.commit()
    return question
