from pydantic import BaseModel
from typing import List


class User(BaseModel):
    first_name: str
    last_name: str = None
    age: int

    class Config:
        orm_mode = True


class Quiz(BaseModel):
    id: int

    class Config:
        orm_mode = True


class Answer(BaseModel):
    text: str

    class Config:
        orm_mode = True


class Question(BaseModel):
    # right_answer_id: int
    question: str
    answers: List[Answer]

    quiz_id: int

    class Config:
        orm_mode = True
